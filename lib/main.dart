import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:whereismycheese/model/core/cheesy_note.dart';
import 'package:whereismycheese/model/constants/app_constants.dart';
import 'package:whereismycheese/providers/notes_provider.dart';
import 'package:whereismycheese/providers/theme_provider.dart';
import 'package:whereismycheese/theme.dart';
import 'package:path_provider/path_provider.dart';
import 'package:whereismycheese/view/screens/home.dart';
import 'package:whereismycheese/view/screens/introduction.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('cheese_logo');

  final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings();

  final InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS,
  );

  final bool result = await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          IOSFlutterLocalNotificationsPlugin>()
      ?.requestPermissions(
        alert: true,
        badge: true,
        sound: true,
      );

  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  final document = await getApplicationDocumentsDirectory();

  Hive.init(document.path);
  Hive.registerAdapter(
    CheesyNoteAdapter(),
  );

  await Hive.openBox<CheesyNote>(AppConstants.cheesyNotes);

  runApp(App());
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final _appThemes = AppThemes();

  // checks if this is the first time user opens app.
  bool isFirstOpen = false;

  Future<bool> checkIsFirstAppOpen() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey(AppConstants.isFirstOpen)) {
      return prefs.getBool(AppConstants.isFirstOpen);
    }

    return await prefs.setBool(AppConstants.isFirstOpen, true);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeProvider>(
          create: (_) => ThemeProvider(
            _appThemes.getLightModeTheme(),
          ),
        ),
        ChangeNotifierProvider<CheesyNotesProvider>(
          create: (_) => CheesyNotesProvider(),
        ),
      ],
      child: FutureBuilder(
        future: checkIsFirstAppOpen(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            return ThemedMaterialApp(snapshot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class ThemedMaterialApp extends StatefulWidget {
  final bool isFirstOpen;

  ThemedMaterialApp(this.isFirstOpen);

  @override
  _ThemedMaterialAppState createState() => _ThemedMaterialAppState();
}

class _ThemedMaterialAppState extends State<ThemedMaterialApp> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<CheesyNotesProvider>(context, listen: false).getAllNotes();
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    final appThemes = AppThemes();

    return MaterialApp(
      theme: theme.getTheme,
      darkTheme: appThemes.getDarkModeTheme(),
      themeMode: ThemeMode.system,
      home: widget.isFirstOpen ? Introduction() : HomeScreen(),
      routes: {
        "/home": (context) => HomeScreen(),
      },
    );
  }
}
