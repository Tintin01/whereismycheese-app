import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppThemes {
  final _primaryColor = Colors.orange;
  final _accentColor = Color(0xFFcbd5cd);

  final _lightIconColor = Colors.grey;
  final _darkIconColor = Colors.grey[200];

  final _canvasColorDark = Color(0xFFfffbf2);
  final _canvasColorLight = Color(0xFFf4f4f4);

  final _cardColorDark = Color(0xFF383a3b);
  final _cardColorLight = Color(0xFFf4fcfc);

  final _iconTheme = IconThemeData(size: 16);

  final _appBarTheme = AppBarTheme(elevation: 0);

  final TextTheme _textTheme = TextTheme(
    headline1: GoogleFonts.montserrat(
        fontSize: 97, fontWeight: FontWeight.w300, letterSpacing: -1.5),
    headline2: GoogleFonts.montserrat(
        fontSize: 61, fontWeight: FontWeight.w300, letterSpacing: -0.5),
    headline3:
        GoogleFonts.montserrat(fontSize: 48, fontWeight: FontWeight.w400),
    headline4: GoogleFonts.montserrat(
        fontSize: 34, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    headline5:
        GoogleFonts.montserrat(fontSize: 24, fontWeight: FontWeight.w400),
    headline6: GoogleFonts.montserrat(
        fontSize: 20, fontWeight: FontWeight.w500, letterSpacing: 0.15),
    subtitle1: GoogleFonts.montserrat(
        fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.15),
    subtitle2: GoogleFonts.montserrat(
        fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1),
    bodyText1: GoogleFonts.montserrat(
        fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
    bodyText2: GoogleFonts.montserrat(
        fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    button: GoogleFonts.montserrat(
        fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),
    caption: GoogleFonts.montserrat(
        fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
    overline: GoogleFonts.montserrat(
        fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
  );

  final BottomNavigationBarThemeData _bottomNavigationBarTheme =
      BottomNavigationBarThemeData(
    backgroundColor: Colors.black,
    selectedIconTheme: IconThemeData(color: Colors.orange),
    unselectedIconTheme: IconThemeData(
      color: Colors.blueGrey[600],
    ),
  );

  Color get primaryColor => _primaryColor;
  BottomNavigationBarThemeData get bottomNavTheme => _bottomNavigationBarTheme;

  ThemeData getLightModeTheme() {
    return ThemeData(
        primaryColor: _primaryColor,
        canvasColor: _canvasColorLight,
        iconTheme: _iconTheme.copyWith(
          color: _lightIconColor,
        ),
        textTheme: _textTheme,
        appBarTheme: _appBarTheme.copyWith(backgroundColor: _canvasColorLight),
        bottomAppBarColor: _canvasColorLight,
        cardColor: _cardColorLight,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        bottomNavigationBarTheme: _bottomNavigationBarTheme);
  }

  ThemeData getDarkModeTheme() {
    return ThemeData(
      primaryColor: Colors.deepPurple[900],
      canvasColor: _canvasColorDark,
      iconTheme: _iconTheme.copyWith(
        color: _darkIconColor,
      ),
      appBarTheme: _appBarTheme.copyWith(
        backgroundColor: _canvasColorDark,
      ),
      bottomAppBarColor: _canvasColorDark,
      cardColor: _cardColorDark,
      textTheme: _textTheme,
      visualDensity: VisualDensity.adaptivePlatformDensity,
    );
  }
}
