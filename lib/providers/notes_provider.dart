import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:whereismycheese/model/core/cheesy_note.dart';
import 'package:whereismycheese/model/services/cheesy_notes_service.dart';

class CheesyNotesProvider with ChangeNotifier {
  final _cheesyNotesService = CheesyNotesService();

  List<CheesyNote> _notes = [];

  List<CheesyNote> get notes => _notes;

  ValueListenable<Box<CheesyNote>> get listenableBox =>
      _cheesyNotesService.listenableBox;

  void getAllNotes() {
    _notes = _cheesyNotesService.getAllNotes();
    print("********** NOTES $_notes");
    notifyListeners();
  }

  void addNote(CheesyNote note) async {
    await _cheesyNotesService.addNote(note);
    getAllNotes();
    notifyListeners();
  }

  Future<void> deleteNote(String noteId) async {
    await _cheesyNotesService.deleteNote(noteId);
    getAllNotes();
    notifyListeners();
  }

  CheesyNote getNoteByLatLng(LatLng latLng) {
    return _cheesyNotesService.getNoteByLatLng(latLng);
  }

  CheesyNote getNoteByeId(String id) {
    return _cheesyNotesService.getNoteById(id);
  }
}
