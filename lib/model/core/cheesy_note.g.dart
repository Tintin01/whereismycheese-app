// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cheesy_note.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CheesyNoteAdapter extends TypeAdapter<CheesyNote> {
  @override
  final int typeId = 0;

  @override
  CheesyNote read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CheesyNote(
      id: fields[0] as String,
      message: fields[1] as String,
      lat: fields[2] as double,
      long: fields[3] as double,
    );
  }

  @override
  void write(BinaryWriter writer, CheesyNote obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.message)
      ..writeByte(2)
      ..write(obj.lat)
      ..writeByte(3)
      ..write(obj.long);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheesyNoteAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
