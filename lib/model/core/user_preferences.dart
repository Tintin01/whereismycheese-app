import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:whereismycheese/theme.dart';

// part 'user_preferences.g.dart';

// @HiveType(typeId: 1)
class UserPreferences {

  @HiveField(0)
  final ThemeData themeMode = AppThemes().getLightModeTheme();
}
