import 'package:hive/hive.dart';

part 'cheesy_note.g.dart';

@HiveType(typeId: 0)
class CheesyNote {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final String message;

  @HiveField(2)
  final double lat;
  
  @HiveField(3)
  final double long;

  CheesyNote({this.id, this.message, this.lat, this.long});
}
