class AppConstants {
  static String cheesyNotes = "cheesy-notes";
  static String themeMode = "theme-mode";
  static String isFirstOpen = "first-open";
}
