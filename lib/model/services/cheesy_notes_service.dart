import 'dart:async';
import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:whereismycheese/model/constants/app_constants.dart';
import 'package:whereismycheese/model/core/cheesy_note.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;

class CheesyNotesService {
  Box<CheesyNote> _box = Hive.box(AppConstants.cheesyNotes);

  ValueListenable<Box<CheesyNote>> get listenableBox => _box.listenable();

  Future<void> addNote(CheesyNote note) async {
    final id = "${note.lat}${note.long}";

    print("RUNNING ADD NOTE LAT IS ${note.lat} LONG IS ${note.long}");

    final geofenceAddSuccess = await bg.BackgroundGeolocation.addGeofence(
      bg.Geofence(
        identifier: id,
        radius: 20,
        latitude: note.lat,
        longitude: note.long,
        notifyOnEntry: true,
        notifyOnExit: true,
      ),
    );

    print("SUCCESSFULLY ADDED GEOFENCE? $geofenceAddSuccess");

    await _box.put(id, note);
  }

  CheesyNote getNoteByLatLng(LatLng latLng) {
    String id = "${latLng.latitude}${latLng.longitude}";

    print("$id ************** ID!!");

    final values = _box.values.toList();

    print(values);
    inspect(values);

    return _box.get(id);
  }

  CheesyNote getNoteByGeofenceId(String id) {
    print("$id ************** ID!!");

    return _box.get(id);
  }

  Future<void> deleteNote(String noteId) async {
    await bg.BackgroundGeolocation.removeGeofence(noteId);
    await _box.delete(noteId);
  }

  CheesyNote getNoteById(String id) {
    return _box.get(id);
  }

  List<CheesyNote> getAllNotes() {
    List<CheesyNote> notes = _box.values.toList();
    return notes;
  }

  Stream<BoxEvent> getNotesStream() {
    return _box.watch();
  }
}
