import 'package:flutter/material.dart';
import 'package:whereismycheese/model/core/cheesy_note.dart';

class DiscoveredNoteDialog extends StatelessWidget {
  final CheesyNote cheesyNote;

  DiscoveredNoteDialog(this.cheesyNote);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          height: 400,
          width: 600,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
              SizedBox(height: 16),
              Center(
                child: Image.asset("assets/images/cheese_wheel.png"),
              ),
              SizedBox(height: 16),
              Text("Cheese Discovered!"),
              SizedBox(height: 16),
              Text(cheesyNote.message),
            ],
          ),
        ),
      ),
    );
  }
}
