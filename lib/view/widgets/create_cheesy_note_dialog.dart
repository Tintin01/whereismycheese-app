import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:whereismycheese/model/core/cheesy_note.dart';
import 'package:whereismycheese/model/services/cheesy_notes_service.dart';

class CreateCheesyNoteDialog extends StatelessWidget {
  final LatLng latLng;

  CreateCheesyNoteDialog(this.latLng);

  final _cheesyMessageController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Future<void> _addCheesyNote(BuildContext context) async {
    final cheesyNotesService = CheesyNotesService();

    if (_formKey.currentState.validate()) {
      final note = CheesyNote(
        id: "${latLng.latitude}${latLng.longitude}",
        message: _cheesyMessageController.text,
        lat: latLng.latitude,
        long: latLng.longitude,
      );

      await cheesyNotesService.addNote(note);

      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final textTheme = Theme.of(context).textTheme;

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          height: 400,
          width: 600,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
              SizedBox(height: 16),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _cheesyMessageController,
                      cursorColor: primaryColor,
                      validator: (String value) {
                        if (value == null) {
                          return "Field cannot be left empty";
                        }
                        return null;
                      },
                      maxLines: 7,
                      decoration: InputDecoration(
                        hintText: "Leave a cheesy note...",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    SizedBox(height: 50),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(
                            width: 2,
                            color: primaryColor,
                          ),
                        ),
                        onPressed: () async => await _addCheesyNote(context),
                        child: Text(
                          "Save Cheese!",
                          style: textTheme.bodyText1.copyWith(
                            color: primaryColor,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
