import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:whereismycheese/model/core/cheesy_note.dart';
import 'package:whereismycheese/providers/notes_provider.dart';
import 'package:whereismycheese/view/widgets/create_cheesy_note_dialog.dart';
import 'package:flutter/services.dart' show rootBundle;

class AppMapView extends StatefulWidget {
  final Position userPosition;

  AppMapView({this.userPosition});

  @override
  _AppMapViewState createState() => _AppMapViewState();
}

class _AppMapViewState extends State<AppMapView> with WidgetsBindingObserver {
  BitmapDescriptor cheeseIcon;
  final Completer<GoogleMapController> _controller = Completer();

  String _darkMapStyle;
  String _lightMapStyle;

  final _defaultCameraPosition = CameraPosition(
    target: LatLng(
      -33.918861,
      18.423300,
    ),
    zoom: 19,
  );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/images/cheese32.png')
        .then((d) {
      setState(() {
        cheeseIcon = d;
      });
    });

    _loadMapStyles();
  }

  @override
  void didChangePlatformBrightness() {
    setState(() {
      _setMapStyle();
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future _setMapStyle() async {
    final controller = await _controller.future;
    final theme = WidgetsBinding.instance.window.platformBrightness;
    if (theme == Brightness.dark) {
      controller.setMapStyle(_darkMapStyle);
    } else {
      controller.setMapStyle(_lightMapStyle);
    }
  }

  Future _loadMapStyles() async {
    _darkMapStyle = await rootBundle.loadString('assets/map_styles/dark.json');
    _lightMapStyle =
        await rootBundle.loadString('assets/map_styles/light.json');
  }

  Future<void> _showCreateCheesyNoteDialog(
      BuildContext context, LatLng latlng) async {
    await showDialog(
      context: context,
      builder: (context) {
        return CreateCheesyNoteDialog(latlng);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final cheesyNotesProvider = Provider.of<CheesyNotesProvider>(context);

    return ValueListenableBuilder<Box<CheesyNote>>(
        valueListenable: cheesyNotesProvider.listenableBox,
        builder: (context, value, _) {
          return GoogleMap(
            onMapCreated: (controller) {
              _controller.complete(controller);
            },
            onLongPress: (LatLng latLng) async =>
                await _showCreateCheesyNoteDialog(context, latLng),
            initialCameraPosition: widget.userPosition != null
                ? CameraPosition(
                    target: LatLng(
                      widget.userPosition.latitude,
                      widget.userPosition.longitude,
                    ),
                    zoom: 19,
                  )
                : _defaultCameraPosition,
            buildingsEnabled: false,
            compassEnabled: true,
            myLocationEnabled: true,
            indoorViewEnabled: false,
            mapToolbarEnabled: false,
            myLocationButtonEnabled: true,
            markers: Set.of(
              value.values.map(
                (CheesyNote note) => Marker(
                  icon: cheeseIcon ?? BitmapDescriptor.defaultMarker,
                  markerId: MarkerId(
                    Random().nextInt(1000).toString(),
                  ),
                  position: LatLng(note.lat, note.long),
                ),
              ),
            ),
          );
        });
  }
}
