import 'package:flutter/material.dart';

class DiscoverNotes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;

    return Container(
      child: Column(
        children: [
          SizedBox(height: 40),
          Text(
            "Discover 🧀 Notes!",
            style: theme.headline4,
          ),
          SizedBox(height: 30),
          SizedBox(
            height: 250,
            width: 300,
            child: Image.asset("assets/images/discover.png"),
          ),
          SizedBox(height: 30),
          Text(
            "Move around! Find notes when you're nearby!",
            style: theme.bodyText2.copyWith(color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
