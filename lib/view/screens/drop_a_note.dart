import 'package:flutter/material.dart';

class DropANote extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;

    return Container(
      child: Column(
        children: [
          SizedBox(height: 40),
          Text(
            "Drop a Note",
            style: theme.headline4,
          ),
          SizedBox(height: 30),
          SizedBox(
            height: 250,
            width: 300,
            child: Image.asset("assets/images/map.png"),
          ),
          SizedBox(height: 30),
          Text(
            "Tap and hold the map to drop your cheesy note! 🧀",
            style: theme.bodyText2.copyWith(color: Colors.grey),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Text(
              "(if your note is dropped too close to you, you may need to distance yourself from the note and come back to rediscover it!)",
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
