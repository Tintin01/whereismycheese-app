import 'package:flutter/material.dart';

class GetNotified extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;

    return Container(
      child: Column(
        children: [
          SizedBox(height: 40),
          Text(
            "Get Note-ified",
            style: theme.headline4,
          ),
          SizedBox(height: 30),
          SizedBox(
            height: 250,
            width: 300,
            child: Image.asset("assets/images/get_notified.png"),
          ),
          SizedBox(height: 30),
          Text(
            "Get notified about nearby notes - even when WhereIsMyCheese isn't open!",
            textAlign: TextAlign.center,
            style: theme.bodyText2.copyWith(color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
