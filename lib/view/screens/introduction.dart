import 'package:flutter/material.dart';
import 'package:whereismycheese/view/screens/discover_note.dart';
import 'package:whereismycheese/view/screens/drop_a_note.dart';
import 'package:whereismycheese/view/screens/get_notified.dart';
import 'package:whereismycheese/view/screens/logo_screen.dart';

class Introduction extends StatefulWidget {
  _IntroductionState createState() => _IntroductionState();
}

class _IntroductionState extends State<Introduction> {
  PageController _pageController;
  int currentIndex = 0;

  final List<Widget> _screens = [
    DropANote(),
    DiscoverNotes(),
    GetNotified(),
    LogoScreen(),
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  onChangedFunction(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  void _navigateHome(BuildContext context) =>
      Navigator.of(context).pushNamed("/home");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          alignment: Alignment.center,
          children: [
            PageView(
              onPageChanged: onChangedFunction,
              controller: _pageController,
              children: _screens,
            ),
            Positioned(
              bottom: 120,
              child: Row(
                children: <Widget>[
                  Indicator(
                    positionIndex: 0,
                    currentIndex: currentIndex,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Indicator(
                    positionIndex: 1,
                    currentIndex: currentIndex,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Indicator(
                    positionIndex: 2,
                    currentIndex: currentIndex,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Indicator(
                    positionIndex: 3,
                    currentIndex: currentIndex,
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(28),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Indicator extends StatelessWidget {
  final int positionIndex, currentIndex;
  const Indicator({this.currentIndex, this.positionIndex});
  @override
  Widget build(BuildContext context) {
    Color _primaryColor = Theme.of(context).primaryColor;
    return Container(
      height: 12,
      width: 12,
      decoration: BoxDecoration(
        color: positionIndex == currentIndex
            ? _primaryColor
            : Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.circular(100),
      ),
    );
  }
}
