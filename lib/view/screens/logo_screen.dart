import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:whereismycheese/model/constants/app_constants.dart';

class LogoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextTheme theme = Theme.of(context).textTheme;

    Future<void> navigateToHomeScreen() async {
      final prefs = await SharedPreferences.getInstance();
      prefs.setBool(AppConstants.isFirstOpen, false);

      Navigator.of(context).pushNamed("/home");
    }

    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          children: [
            SizedBox(height: 60),
            SizedBox(
              height: 250,
              width: 300,
              child: Image.asset("assets/images/cheese_logo.png"),
            ),
            SizedBox(height: 15),
            Text(
              "WhereIsMyCheese",
              textAlign: TextAlign.center,
              style: theme.headline5.copyWith(color: Colors.black),
            ),
            SizedBox(height: 20),
            Text(
              "Powered by",
              textAlign: TextAlign.center,
              style: theme.bodyText2.copyWith(color: Colors.grey),
            ),
            SizedBox(height: 30),
            FlutterLogo(
              size: 40,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async => await navigateToHomeScreen(),
        elevation: 3,
        backgroundColor: Theme.of(context).primaryColor,
        label: Text("Begin!"),
        icon: Icon(
          Icons.forward,
          color: Colors.white,
        ),
      ),
    );
  }
}
