import 'dart:math';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:whereismycheese/providers/notes_provider.dart';
import 'package:whereismycheese/view/widgets/app_map_view.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:whereismycheese/view/widgets/discovered_note_dialog.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Position _userPosition;

  @override
  void initState() {
    super.initState();

    bg.BackgroundGeolocation.onGeofence((bg.GeofenceEvent event) {
      if (event.action == "ENTER") {
        print("ENTERED A GEOFENCE!!!");

        _showDiscoveredCheesyNote(context, event.identifier);
      }
    });

    bg.BackgroundGeolocation.ready(bg.Config(
            desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
            distanceFilter: 10.0,
            stopOnTerminate: false,
            startOnBoot: true,
            debug: true,
            geofenceModeHighAccuracy: true,
            geofenceInitialTriggerEntry: false,
            isMoving: true,
            activityRecognitionInterval: 2500,
            enableHeadless: true,
            logLevel: bg.Config.LOG_LEVEL_VERBOSE))
        .then((bg.State state) {
      if (!state.enabled) {
        bg.BackgroundGeolocation.start();
      }
    });

    _getUserPosition().then((Position position) {
      setState(() {
        _userPosition = position;
      });
    }).catchError((onError) {
      Flushbar(
        message: "Error fetching user location.",
        duration: Duration(seconds: 3),
        flushbarStyle: FlushbarStyle.GROUNDED,
        backgroundColor: Theme.of(context).primaryColor,
      ).show(context);
    });
  }

  Future<Position> _getUserPosition() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      return position;
    } catch (err) {
      Flushbar(
        message:
            "Couldn't find your location! Assuming you're in the mother city :)",
        backgroundColor: Theme.of(context).primaryColor,
        flushbarStyle: FlushbarStyle.GROUNDED,
        duration: Duration(seconds: 4),
        icon: Icon(
          Icons.warning,
          color: Colors.white,
          size: 18,
        ),
      ).show(context);

      return null;
    }
  }

  Future<void> _showDiscoveredCheesyNote(
      BuildContext context, String noteId) async {
    final cheesyNoteProvider =
        Provider.of<CheesyNotesProvider>(context, listen: false);

    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'wheresmycheese', 'Cheesy Note Nearby!', 'cheese nearby 🧐',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      Random().nextInt(1000),
      'WhereIsMyCheese',
      'Cheese Located Nearby, Lookout! 👀🧀',
      platformChannelSpecifics,
    );

    final discoveredNote = cheesyNoteProvider.getNoteByeId(noteId);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DiscoveredNoteDialog(discoveredNote);
      },
    );

    await cheesyNoteProvider.deleteNote(noteId);
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final textTheme = Theme.of(context).textTheme;

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: primaryColor,
            title: Text(
              "WhereIsMyCheese",
              style: textTheme.bodyText1.copyWith(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
          ),
          body: FutureBuilder(
            future: _getUserPosition(),
            builder: (BuildContext context, AsyncSnapshot<Position> snapshot) {
              if (snapshot.hasData) {
                return AppMapView(
                  userPosition: snapshot.data,
                );
              }
              if (snapshot.hasError) {
                return AppMapView();
              }
              return Scaffold(
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/images/cheese_logo.png"),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: CircularProgressIndicator(),
                    )
                  ],
                ),
              );
            },
          )),
    );
  }
}
